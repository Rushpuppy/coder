<?php
/**
 * Created by PhpStorm.
 * User: rushpuppy
 * Date: 02.02.17
 * Time: 19:29
 */

namespace lib;


abstract class BaseApi extends BaseController
{
    // Validation Constants
    CONST SUCCESS = 'success';
    CONST ERROR = 'error';

    /**
     * Default Resgister Routing
     * @param $objApp
     * @param $strRestPath
     */
    abstract static function registerRouting($objApp, $strRestPath);

    /**
     * This Method is used to serialize Arrays to Json Data
     * @param $arrData
     * @return string
     */
    public function serialize($arrData)
    {
        $strJson = json_encode($arrData);
        return $strJson;
    }

    /**
     * This Method extracts a Array Value and if its not existing returning a Default Value
     * @param $arrData
     * @param $strKey
     * @param $strDefault
     * @return mixed
     */
    protected function getArrayValue($arrData, $strKey, $strDefault = '')
    {
        $strValue = $strDefault;
        if(isset($arrData[$strKey])) {
            $strValue = $arrData[$strKey];
        }
        return $strValue;
    }

    /**
     * Wandelt den Medoo DB Code in einen Status um
     * @param $numResult
     * @return array
     */
    protected function getDatabaseStatus($numResult)
    {
        if(is_bool($numResult) && !$numResult) {
            $arrResult = ['status' => $this::ERROR];
        } else {
            $arrResult = ['status' => $this::SUCCESS];
        }

        return $arrResult;
    }
}