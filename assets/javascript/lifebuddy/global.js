

$(function() {
    // Slider Activation
    $('#ex1').slider();
});


/**
 * Switch Content
 * This is for Switching Contents
 * @param elFrom
 * @param elTo
 * @param fncCallback
 */
function switchContent(elFrom, elTo, fncCallback) {
    $('.buddy-front-container').removeClass('buddy-front-container');
    $(elFrom).fadeOut(500, function () {
        $(elTo).fadeIn(500);
        $(elTo).addClass('buddy-front-container');
        if(typeof(fncCallback) == "function") {
            fncCallback();
        }
    });
}

/**
 * Switch to Edit Mode
 * This loads EditMode Elements
 * @param element
 */
function switchToEditMode() {
    $('.buddy-mode-display').fadeOut(500, function () {
        $('.buddy-mode-edit').fadeIn(500);
    })
}

/**
 * Switch to Display Mode
 * This loads DisplayMode Elements
 * @param element
 */
function switchToDisplayMode(fncCallback) {
    $('.buddy-mode-edit').fadeOut(500, function () {
        $('.buddy-mode-display').fadeIn(500);
        if(typeof (fncCallback) == 'function') {
            fncCallback();
        }
    })
}

/**
 * Simple Fade
 * @param elOut
 * @param elIn
 */
function fadeInOut(elOut, elIn) {
    $(elOut + ':visible').fadeOut(500, function () {
        $(elIn + ':hidden').fadeIn(500);
    })
}

/**
 * Confirmation Modal
 * @param strType
 * @param strIcon
 * @param strTitle
 * @param strContent
 */
var showConfirmationModal = function(strType, strTitle, strContent) {
    // Initialize Modal and display
    $('#confirmation-modal').modal("show");
    $('#confirmation-modal').find('.confirmation-content').html(strContent);
    $('#confirmation-modal').find('.confirmation-title').html(strTitle);

    // Show Success Modal
    if(strType == 'success') {
        $('#confirmation-modal').find('.confirmation-success').show();
        $('#confirmation-modal').find('.confirmation-error').hide();
        $('#confirmation-modal').find('.confirmation-neutral').hide();
    }

    // Show Error Modal
    if(strType == 'error') {
        $('#confirmation-modal').find('.confirmation-success').hide();
        $('#confirmation-modal').find('.confirmation-error').show();
        $('#confirmation-modal').find('.confirmation-neutral').hide();
    }

    // Show Neutral Modal
    if(strType == 'neutral') {
        $('#confirmation-modal').find('.confirmation-success').hide();
        $('#confirmation-modal').find('.confirmation-error').hide();
        $('#confirmation-modal').find('.confirmation-neutral').show();
    }
};

/**
 * Öffnet ein Loading Overlay
 */
var showLoader = function() {
    if($('div.loading_overlay').length == 0) {
        var strHtml = '<div class="loading_overlay" style="position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; background-color: #0f192a; opacity: 0.9; z-index: 999;"></div>';
        strHtml += '<div class="loading_overlay" style="position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; z-index: 1000;">';
        strHtml += '<div style="position: absolute; padding: 40px; background-color: #0f192a; width: 200px; border-radius: 20px; left: 50%; margin-left: -100px; top: 50%; margin-top: -100px;">';
        strHtml += '<img src="' + controller.home + 'assets/image/placeholder/ajax-loader.gif">';
        strHtml += '</div>';
        strHtml += '</div>';
        $('html').append(strHtml);
        $('div.loading_overlay').fadeIn(500, 'swing', function() {});
    }
};

/**
 * Schliesst das Loading Overlay
 */
var hideLoader = function() {
    $('div.loading_overlay').fadeOut(500, 'swing', function() {
        $('div.loading_overlay').remove();
    });
};

/**
 * Uploaded File To Base64 Convertion
 * This is needed for REST Uploads via Base64
 * @param callback
 */
File.prototype.convertToBase64 = function(callback){
    var reader = new FileReader();
    reader.onload = function(e) {
        callback(e.target.result)
    };
    reader.onerror = function(e) {
        callback(null);
    };
    reader.readAsDataURL(this);
};

/**
 * Upload EventListener
 * @param objElement
 * @param fncCallback
 */
getBase64Image = function(objElement, fncCallback) {
    var objImage = objElement[0].files[0];
    objImage.convertToBase64(function (strBase64) {
        if (typeof(fncCallback) === 'function') {
            fncCallback(strBase64);
        }
    })
};

/**
 * OnLoad Funktion für alles was GLOBAL ausgeführt werden muss
 */
$(function() {
    runSitebarTimer();
});

/**
 * Sekunden Timer für Die Sitebar
 * Diese Methode ruft sich alle 1 sek selber auf.
 */
function runSitebarTimer() {
    setSitebarTime();
    setInterval(function() {
        setSitebarTime();
    }, 1000);
}

/**
 * Datum Uhrzeit berechnen und ausgeben
 * todo: das soll später mit dem Timestamp vom Server her funktionieren so das die Uhrzeit immer auf den PHP
 * todo: Server angepasst ist. Ich weis noch nicht wie das ganze mit ZeitZonen z.B. beim Reisen funktionieren soll.
 */
function setSitebarTime() {

    var arrMonth = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];
    var arrDay = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"];

    function fncAddDigit(n){n = parseInt(n); return n < 10 ? '0' + n: '' + n;}

    // Day Time
    var objDate = new Date();
    var strMonth = arrMonth[objDate.getMonth()];
    var strDay = arrDay[objDate.getDay()];
    var strPercent = '88.88%';
    var strPercentHtml = '<span class="badge badge-default bg-aqua">' + strPercent + '</span>';
    var strTime = fncAddDigit(objDate.getHours()) + ':' + fncAddDigit(objDate.getMinutes()) + ':' + fncAddDigit(objDate.getSeconds()) + '&nbsp;&nbsp;&nbsp;' + strPercentHtml;
    var strDate = objDate.getDate() + ' ' + strMonth + ' ' + objDate.getFullYear();

    $('p.sidebar-day').html(strDay );
    $('p.sidebar-time').html(strTime);
    $('p.sidebar-date').html(strDate);
}


/**
 * Creating a Unique ID via Javascript
 * @returns {string}
 */
function create_guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

/**
 * Encoding a String to UTF-8
 * @param s
 * @returns {string}
 */
function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
}

/**
 * Decoding String to UTF-8
 * @param s
 * @returns {string}
 */
function decode_utf8(s) {
    return decodeURIComponent(escape(s));
}

/**
 * Decode a Json Object to String
 * @param objJson
 */
function decode_json(objJson) {
    var strJson = JSON.stringify(objJson);
    return strJson;
}

/**
 * Encode a String to JSON object
 * @param strString
 * @returns {object}
 */
function encode_json(strString) {
    var objJson = JSON.parse(strString);
    return objJson;
}

/**
 * Compressing a String with the LZW algorithm.
 * @param strString
 * @returns {string}
 */
function encode_lzw(strString) {
    var dict = {};
    var data = (strString + "").split("");
    var out = [];
    var currChar;
    var phrase = data[0];
    var code = 256;
    for (var i=1; i<data.length; i++) {
        currChar=data[i];
        if (dict['_' + phrase + currChar] != null) {
            phrase += currChar;
        }
        else {
            out.push(phrase.length > 1 ? dict['_'+phrase] : phrase.charCodeAt(0));
            dict['_' + phrase + currChar] = code;
            code++;
            phrase=currChar;
        }
    }
    out.push(phrase.length > 1 ? dict['_'+phrase] : phrase.charCodeAt(0));
    for (var i=0; i<out.length; i++) {
        out[i] = String.fromCharCode(out[i]);
    }
    return out.join("");
}

/**
 * Decompress a LZW Compressed String.
 * @param strCompressed
 * @returns {string}
 */
function decode_lzw(strCompressed) {
    var dict = {};
    var data = (strCompressed + "").split("");
    var currChar = data[0];
    var oldPhrase = currChar;
    var out = [currChar];
    var code = 256;
    var phrase;
    for (var i=1; i<data.length; i++) {
        var currCode = data[i].charCodeAt(0);
        if (currCode < 256) {
            phrase = data[i];
        }
        else {
            phrase = dict['_'+currCode] ? dict['_'+currCode] : (oldPhrase + currChar);
        }
        out.push(phrase);
        currChar = phrase.charAt(0);
        dict['_'+code] = oldPhrase + currChar;
        code++;
        oldPhrase = phrase;
    }
    return out.join("");
}

/**
 * Read all the Upload File Data and Return
 * // todo this only reads the first file and
 * // could bee improved to read multible uploads
 * @param elInput
 * @param boolUseCompress
 * @returns {string}
 */
function getFileUploadData(elInput, boolUseCompress) {
    input = document.getElementById(inputId);
    var reader = new FileReader();
    reader.onload = function (e) {
        base64 = e.target.result;
    };
    return reader.readAsDataURL(input.files[0]);
}

/**
 * Base64 Encode Native script
 * @param encodedData
 * @returns {*}
 */
function base64_decode (encodedData) {
    if (typeof window !== 'undefined') {
        if (typeof window.atob !== 'undefined') {
            return decodeURIComponent(encodeURIComponent(window.atob(encodedData)))
        }
    } else {
        return new Buffer(encodedData, 'base64').toString('utf-8')
    }
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1;
    var o2;
    var o3;
    var h1;
    var h2;
    var h3;
    var h4;
    var bits;
    var i = 0;
    var ac = 0;
    var dec = '';
    var tmpArr = [];
    if (!encodedData) {
        return encodedData
    }
    encodedData += '';
    do {
        // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(encodedData.charAt(i++));
        h2 = b64.indexOf(encodedData.charAt(i++));
        h3 = b64.indexOf(encodedData.charAt(i++));
        h4 = b64.indexOf(encodedData.charAt(i++));
        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
        o1 = bits >> 16 & 0xff
        o2 = bits >> 8 & 0xff
        o3 = bits & 0xff
        if (h3 === 64) {
            tmpArr[ac++] = String.fromCharCode(o1)
        } else if (h4 === 64) {
            tmpArr[ac++] = String.fromCharCode(o1, o2)
        } else {
            tmpArr[ac++] = String.fromCharCode(o1, o2, o3)
        }
    } while (i < encodedData.length)
    dec = tmpArr.join('')
    //return decodeURIComponent(encodeURIComponent(dec.replace(/\0+$/, '')))
    return '';
}

/**
 * Base64 Encode Native script
 * @param stringToEncode
 * @returns {*}
 */
function base64_encode (stringToEncode) {
    if (typeof window !== 'undefined') {
        if (typeof window.btoa !== 'undefined') {
            return window.btoa(decodeURIComponent(encodeURIComponent(stringToEncode)))
        }
    } else {
        return new Buffer(stringToEncode).toString('base64')
    }
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1;
    var o2;
    var o3;
    var h1;
    var h2;
    var h3;
    var h4;
    var bits;
    var i = 0;
    var ac = 0;
    var enc = '';
    var tmpArr = [];
    if (!stringToEncode) {
        return stringToEncode
    }
    stringToEncode = decodeURIComponent(encodeURIComponent(stringToEncode));
    do {
        // pack three octets into four hexets
        o1 = stringToEncode.charCodeAt(i++);
        o2 = stringToEncode.charCodeAt(i++);
        o3 = stringToEncode.charCodeAt(i++);
        bits = o1 << 16 | o2 << 8 | o3;
        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;
        // use hexets to index into b64, and append result to encoded string
        tmpArr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4)
    } while (i < stringToEncode.length);
    enc = tmpArr.join('');
    var r = stringToEncode.length % 3;
    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3)
}

/**
 * This Clears a Javasctipt Object but just the
 * values and not the Keys.
 * @param objObject
 * @returns {*}
 */
function clearObject(objObject) {
    for(var strIndex in objObject) {
        if(objObject[strIndex] == 'array') {
            objObject[strIndex] = [];
        } else if(typeof(objObject[strIndex]) == 'object') {
            objObject[strIndex] = [];
        } else {
            objObject[strIndex] = '';
        }
    }

    return objObject;
}

/**
 * File Upload Modal öffnen
 */
function showFileUpload(objOptions, fncCloseCallback) {
    $('#upload-modal').modal();
    $('#upload_modal_picker').show();
    $('#upload_modal_preview').hide();

    $('#upload_modal_file').on('change', function (event) {
        // Create new Image Input
        getBase64Image($('#upload_modal_file'), function (strBase64) {
            $('#upload_modal_picker').hide();
            $('#upload_modal_preview').show();
            $('#upload_modal_img_preview').attr('src', URL.createObjectURL(event.target.files[0]));
            $('#upload_modal_img_preview').attr('base64', strBase64);

        });
    });

    // On Close Return Image
    $('#upload-modal').on('hide.bs.modal', function() {
        if(typeof(fncCloseCallback) == 'function') {
            var strBase64 = $('#upload_modal_img_preview').attr('base64');
            var strImgSrc = $('#upload_modal_img_preview').attr('src');
            fncCloseCallback(strBase64, strImgSrc);
        }
    });
}


