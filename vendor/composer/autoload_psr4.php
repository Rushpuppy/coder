<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/type-resolver/src', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Twig\\Extensions\\' => array($vendorDir . '/twig/extensions/src'),
    'Symfony\\Polyfill\\Php70\\' => array($vendorDir . '/symfony/polyfill-php70'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\PropertyAccess\\' => array($vendorDir . '/symfony/property-access'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Symfony\\Component\\Intl\\' => array($vendorDir . '/symfony/intl'),
    'Symfony\\Component\\Inflector\\' => array($vendorDir . '/symfony/inflector'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Form\\' => array($vendorDir . '/symfony/form'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Bridge\\Twig\\' => array($vendorDir . '/symfony/twig-bridge'),
    'Slim\\Views\\' => array($vendorDir . '/slim/twig-view/src'),
    'Slim\\' => array($vendorDir . '/slim/slim/Slim'),
    'React\\Stream\\' => array($vendorDir . '/react/stream/src'),
    'React\\Socket\\' => array($vendorDir . '/react/socket/src'),
    'React\\Promise\\' => array($vendorDir . '/react/promise/src'),
    'React\\EventLoop\\' => array($vendorDir . '/react/event-loop/src'),
    'ReCaptcha\\' => array($vendorDir . '/google/recaptcha/src/ReCaptcha'),
    'Ratchet\\' => array($vendorDir . '/cboden/ratchet/src/Ratchet'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
);
