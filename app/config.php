<?php

/**
 * Configuration of the Page Header
 * This are the Values that will be published in the Page header Section of the HTML
 * Page. This Array is passed over to Twig and to the Session
 */
$arrConfig['page_header'] = [
    'html_lang' => 'en',
    'meta_charset' => 'utf-8',
    'meta_description' => 'CodeBot is WebApp Template for Quick starting a new Application without use time for stuff i dont want to do.',
    'meta_keywords' => 'WebApp Framework',
    'meta_application-name' => 'CodeBot',
    'meta_author' => 'Severin Holm, Rushpuppy.ch',
    'page_title' => 'CodeBot - by Rushpuppy.ch'
];


/**
 * DataBase Configuration for the Developement environment.
 * When you work on Localhost this are the configs that are used to connect
 * to the Database.
 */
$arrDbConfig['dev_80'] = [
    'database_type' => 'mysql',
    'database_name' => 'lifebuddy',
    'server' => 'localhost',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'port' => 3306
];

$arrDbConfig['dev_8888'] = [
    'database_type' => 'mysql',
    'database_name' => 'lifebuddy',
    'server' => 'localhost',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'port' => 8889
];


/**
 * DataBase Configuration for the Live environment.
 * When the Project is online this are the configs that are used to connect
 * to the Database.
 */
$arrDbConfig['live'] = [
    'database_type' => 'mysql',
    'database_name' => 'coder',
    'server' => 'localhost',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'port' => 3306
];


/**
 * Public Navigation
 * This is the Navigation Menu for all Public Pages. This Array is passed over to Twig and to the Session
 */
$arrConfig['public_navigation']['left'] = [];
$arrConfig['public_navigation']['right'] = [];


/**
 * Logedin User Navigation
 * This is the Navigation Menu for all User Pages. This Array is passed over to Twig and to the Session
 */
$arrConfig['user_navigation']['top'] = [];
$arrConfig['user_navigation']['left'] = [
    '0' => ['title' => 'My Life', 'children' => [
            '0' => ['text' => 'Working', 'icon' => 'fa fa-dashboard', 'linkto' => 'home', 'children' => [
                '0' => ['text' => 'Today', 'icon' => 'fa fa-calendar-o', 'linkto' => 'working_today'],
                '3' => ['text' => 'Review', 'icon' => 'fa fa-eye', 'linkto' => 'working_review'],
                '4' => ['text' => 'Summary', 'icon' => 'fa fa-flag-checkered', 'linkto' => 'working_summary'],
                '5' => ['text' => 'Personal Brand', 'icon' => 'fa fa-user-circle-o', 'linkto' => 'working_branding']
                ]
            ],
            '1' => ['text' => 'Inbox', 'icon' => 'fa fa-inbox', 'linkto' => 'inbox'],
            '2' => ['text' => 'Calendar', 'icon' => 'fa fa-calendar', 'linkto' => 'calendar'],
            '3' => ['text' => 'Resources', 'icon' => 'fa fa-star', 'linkto' => 'home', 'children' => [
                    '0' => ['text' => 'Contacts', 'icon' => 'fa fa-address-card-o', 'linkto' => 'resource_contact'],
                    '1' => ['text' => 'Finances', 'icon' => 'fa fa-money', 'linkto' => 'resource_finance'],
                    '2' => ['text' => 'Time', 'icon' => 'fa fa-clock-o', 'linkto' => 'resource_time'],
                    '3' => ['text' => 'Skills', 'icon' => 'fa fa-trophy', 'linkto' => 'resource_skill'],
                    '4' => ['text' => 'Action-Plans', 'icon' => 'fa fa-puzzle-piece', 'linkto' => 'resource_actionplan'],
                    '5' => ['text' => 'Documents', 'icon' => 'fa fa-archive', 'linkto' => 'resource_document']
                ]
            ]
        ]
    ],

    '1' => ['title' => 'Live Spaces', 'children' => [
            '0' => ['text' => 'Business', 'color' => 'text-aqua', 'icon' => 'fa fa-building', 'linkto' => 'home', 'children' => [
                    '0' => ['text' => 'Activity', 'color' => 'text-aqua', 'icon' => 'fa fa-bolt', 'linkto' => 'lifespace_activity_business'],
                    '1' => ['text' => 'Meetings', 'color' => 'text-aqua', 'icon' => 'fa fa-calendar-check-o', 'linkto' => 'lifespace_meeting_business'],
                    '2' => ['text' => 'Tasks', 'color' => 'text-aqua', 'icon' => 'fa fa-ticket', 'linkto' => 'lifespace_task_business'],
                    '3' => ['text' => 'Projects', 'color' => 'text-aqua', 'icon' => 'fa fa-sitemap', 'linkto' => 'lifespace_project_business'],
                    '4' => ['text' => 'Delegates', 'color' => 'text-aqua', 'icon' => 'fa fa-handshake-o', 'linkto' => 'lifespace_delegation_business'],
                    '5' => ['text' => 'Trackers', 'color' => 'text-aqua', 'icon' => 'fa fa-bullseye', 'linkto' => 'lifespace_tracker_business'],
                    '6' => ['text' => 'Incubator', 'color' => 'text-aqua', 'icon' => 'fa fa-magic', 'linkto' => 'lifespace_incubator_business']
                ]
            ],
            '1' => ['text' => 'Education', 'color' => 'text-green', 'icon' => 'fa fa-graduation-cap', 'linkto' => 'home', 'children' => [
                    '0' => ['text' => 'Activity', 'color' => 'text-green', 'icon' => 'fa fa-bolt', 'linkto' => 'lifespace_activity_education'],
                    '1' => ['text' => 'Meetings', 'color' => 'text-green', 'icon' => 'fa fa-calendar-check-o', 'linkto' => 'lifespace_meeting_education'],
                    '2' => ['text' => 'Tasks', 'color' => 'text-green', 'icon' => 'fa fa-ticket', 'linkto' => 'lifespace_task_education'],
                    '3' => ['text' => 'Projects', 'color' => 'text-green', 'icon' => 'fa fa-sitemap', 'linkto' => 'lifespace_project_education'],
                    '4' => ['text' => 'Delegates', 'color' => 'text-green', 'icon' => 'fa fa-handshake-o', 'linkto' => 'lifespace_delegation_education'],
                    '5' => ['text' => 'Trackers', 'color' => 'text-green', 'icon' => 'fa fa-bullseye', 'linkto' => 'lifespace_tracker_education'],
                    '6' => ['text' => 'Incubator', 'color' => 'text-green', 'icon' => 'fa fa-magic', 'linkto' => 'lifespace_incubator_education']
                ]
            ],
            '2' => ['text' => 'Health', 'color' => 'text-red', 'icon' => 'fa fa-heartbeat', 'linkto' => 'home', 'children' => [
                    '0' => ['text' => 'Activity', 'color' => 'text-red', 'icon' => 'fa fa-bolt', 'linkto' => 'lifespace_activity_health'],
                    '1' => ['text' => 'Meetings', 'color' => 'text-red', 'icon' => 'fa fa-calendar-check-o', 'linkto' => 'lifespace_meeting_health'],
                    '2' => ['text' => 'Tasks', 'color' => 'text-red', 'icon' => 'fa fa-ticket', 'linkto' => 'lifespace_task_health'],
                    '3' => ['text' => 'Projects', 'color' => 'text-red', 'icon' => 'fa fa-sitemap', 'linkto' => 'lifespace_project_health'],
                    '4' => ['text' => 'Delegates', 'color' => 'text-red', 'icon' => 'fa fa-handshake-o', 'linkto' => 'lifespace_delegation_health'],
                    '5' => ['text' => 'Trackers', 'color' => 'text-red', 'icon' => 'fa fa-bullseye', 'linkto' => 'lifespace_tracker_health'],
                    '6' => ['text' => 'Incubator', 'color' => 'text-red', 'icon' => 'fa fa-magic', 'linkto' => 'lifespace_incubator_health']
                ]
            ],
            '3' => ['text' => 'Household', 'color' => 'text-yellow', 'icon' => 'fa fa-home', 'linkto' => 'home', 'children' => [
                    '0' => ['text' => 'Activity', 'color' => 'text-yellow', 'icon' => 'fa fa-bolt', 'linkto' => 'lifespace_activity_household'],
                    '1' => ['text' => 'Meetings', 'color' => 'text-yellow', 'icon' => 'fa fa-calendar-check-o', 'linkto' => 'lifespace_meeting_household'],
                    '2' => ['text' => 'Tasks', 'color' => 'text-yellow', 'icon' => 'fa fa-ticket', 'linkto' => 'lifespace_task_household'],
                    '3' => ['text' => 'Projects', 'color' => 'text-yellow', 'icon' => 'fa fa-sitemap', 'linkto' => 'lifespace_project_household'],
                    '4' => ['text' => 'Delegates', 'color' => 'text-yellow', 'icon' => 'fa fa-handshake-o', 'linkto' => 'lifespace_delegation_household'],
                    '5' => ['text' => 'Trackers', 'color' => 'text-yellow', 'icon' => 'fa fa-bullseye', 'linkto' => 'lifespace_tracker_household'],
                    '6' => ['text' => 'Incubator', 'color' => 'text-yellow', 'icon' => 'fa fa-magic', 'linkto' => 'lifespace_incubator_household']
                ]
            ],
            '4' => ['text' => 'Leisure', 'color' => 'text-fuchsia', 'icon' => 'fa fa-tree', 'linkto' => 'home', 'children' => [
                    '0' => ['text' => 'Activity', 'color' => 'text-fuchsia', 'icon' => 'fa fa-bolt', 'linkto' => 'lifespace_activity_leisure'],
                    '1' => ['text' => 'Meetings', 'color' => 'text-fuchsia', 'icon' => 'fa fa-calendar-check-o', 'linkto' => 'lifespace_meeting_leisure'],
                    '2' => ['text' => 'Tasks', 'color' => 'text-fuchsia', 'icon' => 'fa fa-ticket', 'linkto' => 'lifespace_task_leisure'],
                    '3' => ['text' => 'Projects', 'color' => 'text-fuchsia', 'icon' => 'fa fa-sitemap', 'linkto' => 'lifespace_project_leisure'],
                    '4' => ['text' => 'Delegates', 'color' => 'text-fuchsia', 'icon' => 'fa fa-handshake-o', 'linkto' => 'lifespace_delegation_leisure'],
                    '5' => ['text' => 'Trackers', 'color' => 'text-fuchsia', 'icon' => 'fa fa-bullseye', 'linkto' => 'lifespace_tracker_leisure'],
                    '6' => ['text' => 'Incubator', 'color' => 'text-fuchsia', 'icon' => 'fa fa-magic', 'linkto' => 'lifespace_incubator_leisure']
                ]
            ]
        ]
    ],
];

/**
 * WebSocket Server Configuration
 */
$arrConfig['web_socket'] = [
    '8110' => 'WebSocketDemo', 
];
