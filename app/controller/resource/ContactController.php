<?php


namespace app\controller\resource;
use lib\BaseController;

/**
 * ContactController
 * This controller is managing the Resource Contact Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class ContactController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Inbox
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('resource_contact');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'resource/contact.html',
            'vue' => 'resource/contact.js',
            'components' => [
                'RestModel',
                'GoogleMap'
            ]
        ]);
    }
}