<?php


namespace app\controller\resource;
use lib\BaseController;

/**
 * SkillController
 * This controller is managing the Resource Skill Page
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class SkillController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Skills
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('resource_skill');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'resource/skill.html',
            'vue' => 'resource/skill.js',
        ]);
    }
}