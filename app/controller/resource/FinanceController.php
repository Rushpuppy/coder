<?php


namespace app\controller\resource;
use lib\BaseController;

/**
 * FinanceController
 * This controller is managing the Resource Finance Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class FinanceController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Finances
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('resource_finance');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'resource/finance.html',
            'vue' => 'resource/finance.js',
        ]);
    }
}