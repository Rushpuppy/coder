<?php


namespace app\controller\resource;
use lib\BaseController;

/**
 * ContactController
 * This controller is managing the Resource Document Page
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class DocumentController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Documents
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('resource_document');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'resource/document.html',
            'vue' => 'resource/document.js',
        ]);
    }
}