<?php


namespace app\controller\resource;
use lib\BaseController;

/**
 * ActionPlanController
 * This controller is managing the Resource ActionPlan Page
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class ActionPlanController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for ActionPlan
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('resource_actionplan');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'resource/actionplan.html',
            'vue' => 'resource/actionplan.js',
        ]);
    }
}