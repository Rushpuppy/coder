<?php


namespace app\controller\resource;
use lib\BaseController;

/**
 * InboxController
 * This controller is managing the Resource Time Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class TimeController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Inbox
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('resource_time');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'resource/time.html',
            'vue' => 'resource/time.js',
            'components' => ['RestModel']
        ]);
    }
}