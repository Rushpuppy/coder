<?php

namespace app\controller\lifespace;
use lib\BaseApi;
use lib\BaseController;

/**
 * ActivityController
 * This controller is managing the Lifespace Activity Page
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class ActivityController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Activity
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $strContext = $this->getLifespace($request);
        $this->setContext('lifespace_activity_' . strtolower($strContext));

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'lifespace/activity.html',
            'vue' => 'lifespace/activity.js',
            'lifespace' => $strContext
        ]);
    }

    /**
     * Get Lifespace from URI
     * @param $request
     * @return string
     */
    protected function getLifespace($request) {
        $strUrl = $request->getUri();
        $arrUrl = explode('/', $strUrl);

        $strReturn = 'lifespace';
        switch($arrUrl[count($arrUrl) - 1]) {
            case 'business':
                $strReturn = 'Business';
                break;
            case 'education':
                $strReturn = 'Education';
                break;
            case 'health':
                $strReturn = 'Health';
                break;
            case 'household':
                $strReturn = 'Household';
                break;
            case 'leisure':
                $strReturn = 'Leisure';
                break;
        }

        return $strReturn;
    }
}