<?php


namespace app\controller\working;
use lib\BaseController;

/**
 * TodayController
 * This controller is managing the Working Today Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class TodayController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Today
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('work_today');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'working/today.html',
            'vue' => 'working/today.js',
        ]);
    }
}