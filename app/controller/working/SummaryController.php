<?php


namespace app\controller\working;
use lib\BaseController;

/**
 * TodayController
 * This controller is managing the Working Summary Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class SummaryController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Today
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('work_summary');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'working/summary.html',
            'vue' => 'working/summary.js',
        ]);
    }
}