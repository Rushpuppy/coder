<?php


namespace app\controller\working;
use lib\BaseController;

/**
 * BrandingController
 * This controller is managing the Working Branding Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class BrandingController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Today
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('work_branding');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'working/branding.html',
            'vue' => 'working/branding.js',
        ]);
    }
}