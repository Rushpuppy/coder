<?php


namespace app\controller\working;
use lib\BaseController;

/**
 * ReviewController
 * This controller is managing the Working Review Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class ReviewController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Review
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('work_review');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'working/review.html',
            'vue' => 'working/review.js',
        ]);
    }
}