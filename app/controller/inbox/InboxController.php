<?php

namespace app\controller\inbox;
use lib\BaseController;

/**
 * InboxController
 * This controller is managing the Inbox Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class InboxController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Inbox
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('inbox');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'inbox/inbox.html',
            'vue' => 'inbox/inbox.js',
        ]);
    }
}