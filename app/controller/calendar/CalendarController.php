<?php

namespace app\controller\calendar;
use lib\BaseController;

/**
 * CalendarController
 * This controller is managing the Lifespace Calendar Page
 * The User Setting Page.
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
class CalendarController extends BaseController
{
    /**
     * Index Method
     * Loading HTML Template and Javascript for Inbox
     * @param $request
     * @param $response
     * @param $args
     */
    public function index($request, $response, $args)
    {
        // Set Context
        $this->setContext('calendar');

        // Render Settings View
        $this->view->render($response, 'index.html', [
            'page' => 'calendar/calendar.html',
            'vue' => 'calendar/calendar.js',
        ]);
    }
}