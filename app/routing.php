<?php

/**
 * INDEX Page Routing
 * This just sends you forward to the Public Page Scope
 */
$app->get('/', function ($request, $response, $args) {
    return $response->withRedirect('home/index/ok');
})->setName('home');


/**
 * Public Page Routing
 * This routing belong to the public zone so there is no session and no User
 * In this Case we have 2 Controller one for Guests and one for users
 */
$app->get('/home/index/{code}', 'HomeController:index')->setName('home_index');


/**
 * Loggin Mechanism Routing
 * Here is the complete login mechanism routing. So everything that has todo with
 * the login process comes in here.
 */
$app->post('/account/login', 'AccountService:login')->setName('account_login');
$app->post('/account/signup', 'AccountService:signup')->setName('account_signup');
$app->get('/account/activate/{code}', 'AccountService:activate')->setName('account_activate');
$app->post('/account/recovery', 'AccountService:recovery')->setName('account_recovery');
$app->get('/account/logout', 'AccountService:logout')->setName('account_logout');
$app->get('/account/setrecovery/{code}', 'AccountService:setRecovery')->setName('account_set_recovery');
$app->post('/account/changeprofile', 'AccountService:changeProfile')->setName('account_change_profile');
$app->post('/account/changepassword', 'AccountService:changePassword')->setName('account_change_password');
$app->post('/account/sendchangeemail', 'AccountService:sendChangeEmail')->setName('account_send_change_email');
$app->get('/account/changeemail/{code}', 'AccountService:changeEmail')->setName('account_change_email');
$app->post('/account/deleteaccount/', 'AccountService:deleteAccount')->setName('account_delete');


/**
 * Error Page Routing
 * Thats the error page routing so if you have your own error codes or the
 * user gets one of the official errors like 404 the routing is in here.
 */
// TODO:


/**
 * My Life Routing Rules
 * This are the routing rules for everything inside the "My Life" part of the navigation
 * This Pages are all visible and useable for logged in users
 */
$app->get('/working/today', 'TodayController:index')->add(new \lib\AuthMiddleware())->setName('working_today');
$app->get('/working/review', 'ReviewController:index')->add(new \lib\AuthMiddleware())->setName('working_review');
$app->get('/working/summary', 'SummaryController:index')->add(new \lib\AuthMiddleware())->setName('working_summary');
$app->get('/working/branding', 'BrandingController:index')->add(new \lib\AuthMiddleware())->setName('working_branding');
$app->get('/inbox/', 'InboxController:index')->add(new \lib\AuthMiddleware())->setName('inbox');
$app->get('/calendar/', 'CalendarController:index')->add(new \lib\AuthMiddleware())->setName('calendar');
$app->get('/resource/contact', 'ContactController:index')->add(new \lib\AuthMiddleware())->setName('resource_contact');
$app->get('/resource/finacne', 'FinanceController:index')->add(new \lib\AuthMiddleware())->setName('resource_finance');
$app->get('/resource/time', 'TimeController:index')->add(new \lib\AuthMiddleware())->setName('resource_time');
$app->get('/resource/skill', 'SkillController:index')->add(new \lib\AuthMiddleware())->setName('resource_skill');
$app->get('/resource/actionplan', 'ActionPlanController:index')->add(new \lib\AuthMiddleware())->setName('resource_actionplan');
$app->get('/resource/document', 'DocumentController:index')->add(new \lib\AuthMiddleware())->setName('resource_document');

// lifespace Container
$app->get('/lifespace/activity/business', 'ActivityController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_activity_business');
$app->get('/lifespace/activity/education', 'ActivityController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_activity_education');
$app->get('/lifespace/activity/health', 'ActivityController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_activity_health');
$app->get('/lifespace/activity/household', 'ActivityController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_activity_household');
$app->get('/lifespace/activity/leisure', 'ActivityController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_activity_leisure');

$app->get('/lifespace/delegation/business', 'DelegationController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_delegation_business');
$app->get('/lifespace/delegation/education', 'DelegationController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_delegation_education');
$app->get('/lifespace/delegation/health', 'DelegationController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_delegation_health');
$app->get('/lifespace/delegation/household', 'DelegationController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_delegation_household');
$app->get('/lifespace/delegation/leisure', 'DelegationController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_delegation_leisure');

$app->get('/lifespace/incubator/business', 'IncubatorController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_incubator_business');
$app->get('/lifespace/incubator/education', 'IncubatorController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_incubator_education');
$app->get('/lifespace/incubator/health', 'IncubatorController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_incubator_health');
$app->get('/lifespace/incubator/household', 'IncubatorController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_incubator_household');
$app->get('/lifespace/incubator/leisure', 'IncubatorController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_incubator_leisure');

$app->get('/lifespace/meeting/business', 'MeetingController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_meeting_business');
$app->get('/lifespace/meeting/education', 'MeetingController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_meeting_education');
$app->get('/lifespace/meeting/health', 'MeetingController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_meeting_health');
$app->get('/lifespace/meeting/household', 'MeetingController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_meeting_household');
$app->get('/lifespace/meeting/leisure', 'MeetingController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_meeting_leisure');

$app->get('/lifespace/project/business', 'ProjectController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_project_business');
$app->get('/lifespace/project/education', 'ProjectController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_project_education');
$app->get('/lifespace/project/health', 'ProjectController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_project_health');
$app->get('/lifespace/project/household', 'ProjectController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_project_household');
$app->get('/lifespace/project/leisure', 'ProjectController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_project_leisure');

$app->get('/lifespace/task/business', 'TaskController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_task_business');
$app->get('/lifespace/task/education', 'TaskController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_task_education');
$app->get('/lifespace/task/health', 'TaskController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_task_health');
$app->get('/lifespace/task/household', 'TaskController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_task_household');
$app->get('/lifespace/task/leisure', 'TaskController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_task_leisure');

$app->get('/lifespace/tracker/business', 'TrackerController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_tracker_business');
$app->get('/lifespace/tracker/education', 'TrackerController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_tracker_education');
$app->get('/lifespace/tracker/health', 'TrackerController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_tracker_health');
$app->get('/lifespace/tracker/household', 'TrackerController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_tracker_household');
$app->get('/lifespace/tracker/leisure', 'TrackerController:index')->add(new \lib\AuthMiddleware())->setName('lifespace_tracker_leisure');

/**
 * App Routings
 * Here comes your application Routing for every page you do.
 */
// TODO: Enter your Routing Rules here!

/**
 * REST API Routing
 * Here cones your REST Api Routing Controllers
 */
\app\api\TimeApi::registerRouting($app, '/api/time');
\app\api\ContactApi::registerRouting($app, '/api/contact');