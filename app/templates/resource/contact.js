

var controller = new Vue({
    delimiters: ['[[', ']]'],
    el: '#page_container',
    data: {
        contacts: [],
        contact: {},
        char_counts: {
            left: [],
            right: []
        },
        filter: {
            'char': '',
            'show_person': true,
            'show_company': true
        },
        map: []
    },
    methods: {
        /**
         * Init Methode
         */
        init: function() {
            // Init Form
            controller.eventHandler();
            controller.loadContactList();

            // Init DatePicker
            $('.datepicker').datepicker();
        },

        /**
         * EventHandler Methode
         */
        eventHandler: function() {
        },

        /**
         * REST Methode
         * Laden aller Kontakte
         */
        loadContactList: function() {
            showLoader();
            var strFilterBase64 = base64_encode(decode_json(controller.filter));
            var objRest = new RestModelComponent({rest_url: controller.home + 'api/contact'});
            objRest.readByFieldValue('filter', strFilterBase64, function (objResponse, strStatus, jqXHR) {
                hideLoader();
                var objData = objRest.getData();
                controller.contacts = objData.contacts;
                controller.char_counts = objData.char_counts;
            });
        },

        /**
         * REST Methode
         * Laden von Detail Informationen
         */
        loadContactDetail: function(numId) {
            showLoader();
            var objRest = new RestModelComponent({rest_url: controller.home + 'api/contact'});
            objRest.readByFieldValue('id', numId, function (objResponse, strStatus, jqXHR) {
                hideLoader();
                controller.contact = objRest.getData();

                // Show Contact Type
                var strType = controller.contact.type;
                $('.contact-type').hide();
                $('.contact-type-' + strType).show();
            });
        },

        /**
         * REST Methode
         * Laden eines Leeren Contacts
         */
        loadEmptyContact: function() {
            showLoader();
            var objRest = new RestModelComponent({rest_url: controller.home + 'api/contact/empty'});
            objRest.readAll(function() {
                hideLoader();
                controller.contact = objRest.getData();
            });
        },

        /**
         * Loading List View
         */
        showList: function() {
            // Container Switch
            fadeInOut('#view_detail_navigation', '#view_list_container');
            fadeInOut('.function_navigation', '#function_listview, #function_filtering');
            switchContent('#view_detail_container', '#view_list_container');
            switchToDisplayMode();
        },

        /**
         * Loading and Showing Detail View
         */
        showDetail: function(event) {
            // Container Switch
            fadeInOut('#view_list_container', '#view_detail_navigation');
            fadeInOut('.function_navigation', '#function_detail');
            switchContent('#view_list_container', '#view_detail_container');
            switchToDisplayMode(function() {
                controller.renderGoogleMap();
            });

            // Load Contact Data
            var numId = $(event.target).closest('[data-id]').data('id');
            controller.loadContactDetail(numId);
        },

        /**
         * Add New Contact
         */
        showAddNew: function() {
            // Container Switch
            fadeInOut('#view_list_container', '#view_detail_navigation');
            fadeInOut('.function_navigation', '#function_addmode');
            switchContent('#view_list_container', '#view_detail_container');
            switchToEditMode();
            $('.contact-type').hide();
            $('.contact-type-person').show();

            // Load Empty Contact Data
            controller.loadEmptyContact();
        },

        /**
         * Converts Contact to Company
         * Only in Add New Mode
         */
        transformToCompany: function() {
            fadeInOut('.contact-type-person', '.contact-type-company');
            controller.contact.type = 'company';
        },

        /**
         * Converts Contact to Person
         * Only in Add New Mode
         */
        transformToPerson: function() {
            fadeInOut('.contact-type-company', '.contact-type-person');
            controller.contact.type = 'person';
        },

        /**
         * Load Edit Mode
         */
        showEditMode: function() {
            fadeInOut('.function_navigation', '#function_editmode');
            switchToEditMode();
        },

        /**
         * Save Contact Informations
         */
        saveDetail: function() {
            showLoader();
            var objRest = new RestModelComponent({rest_url: controller.home + 'api/contact'});

            // Create new Contact
            if(controller.contact.id == '' || typeof(controller.contact.id) == 'undefined') {
                objRest.create(controller.contact, function (objResponse) {
                    hideLoader();
                    controller.contact.id = objResponse.contact_id;
                    controller.contact.image_base64 = null;
                    showConfirmationModal('success', 'Speichern', 'Neuer Kontakt erfasst.');
                });
            }

            // Update Contact
            if(controller.contact.id != '') {
                objRest.update(controller.contact.id, controller.contact, function (objResponse) {
                    hideLoader();
                    controller.contact.image_base64 = null;
                    showConfirmationModal('success', 'Speichern', 'Änderungen gespeichert.');
                });
            }
        },

        /**
         * Deleting a selected Contact
         */
        deleteContact: function() {

        },

        /**
         * Adding a New Social Media Profile
         */
        addSocialMedia: function() {
            var objSocialMedia = {
                calc_platform: 'facebook',
                calc_user: ''
            };
            controller.contact.social_media.push(objSocialMedia);
        },

        /**
         * Removes a Social Media Platform from a Contact
         * @param $event
         */
        removeSocialMedia: function($event) {
            var numIndex = $($event.target).closest('tr').data('id');
            controller.contact.social_media.splice(numIndex, 1);
        },

        /**
         * Adding a New Contact Information
         */
        addContactInfo: function() {
            var objInfo = {
                info_type: 'mobile',
                info_value: ''
            };
            controller.contact.contact_info.push(objInfo);
        },

        /**
         * Removes Contact Information from a Contact
         * @param $event
         */
        removeContactInfo: function($event) {
            var numIndex = $($event.target).closest('tr').data('id');
            controller.contact.contact_info.splice(numIndex, 1);
        },

        /**
         * Setting a Filter Option
         * @param strKey
         * @param strValue
         */
        setFilter: function(strKey, strValue) {
            if(strKey == 'char') {
                if(controller.filter[strKey] == strValue) {
                    controller.filter[strKey] = '';
                } else {
                    controller.filter[strKey] = strValue;
                }
            }
            if(strKey == 'show_person' || strKey == 'show_company') {
                if(controller.filter[strKey] == true) {
                    controller.filter[strKey] = false;
                } else {
                    controller.filter[strKey] = true;
                }
                if(controller.filter[strKey] == false) {
                    if(strKey == 'show_person') {
                        controller.filter['show_company'] = true;
                    } else {
                        controller.filter['show_person'] = true;
                    }
                }
            }

            // Run Filtering
            controller.loadContactList();
        },

        /**
         * Image Upload Handler
         */
        uploadImage: function() {
            showFileUpload({
                'type':'image',
                'width': '250px',
                'height': '250px'
            }, function(strBase64, strImgSrc) {
                $('#profile_img').attr('src', strImgSrc);
                controller.contact.image_base64 = strBase64;
            });
        },

        /**
         * Render Google Maps
         */
        renderGoogleMap: function() {
            var objMapParams = {
                selector: '#google_map_container',
                mapOptions: {
                    disableDefaultUI: true,
                    zoomControl: false,
                    scrollwheel: false,
                    disableDoubleClickZoom: true
                }
            };

            var strAddress = '';
            if(controller.contact.type == 'company') {
                strAddress += ', ' + controller.contact.company_name;
            }
            strAddress += ', ' + controller.contact.address_street;
            strAddress += ', ' + controller.contact.address_additional;
            strAddress += ', ' + controller.contact.address_country + ' - ' + controller.contact.address_postalcode + ' ' + controller.contact.address_city;
            strAddress += ', ' + controller.contact.address_region;

            //controller.map = new GoogleMapComponent(objMapParams);
            //controller.map.setMapCenterByAddress(strAddress);
        }
    }
});

$(function() {
    controller.init();
});
