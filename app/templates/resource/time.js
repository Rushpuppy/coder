
var controller = new Vue({
    delimiters: ['[[', ']]'],
    el: '#page_container',
    data: {
        weeks: [],
        actual_week: 0
    },
    methods: {
        /**
         * Init Methode
         */
        init: function() {
            controller.eventHandler();
            controller.loadWeeks();
        },

        /**
         * EventHandler Methode
         */
        eventHandler: function() {
        },

        /**
         * Rest Methode
         * Laden aller Wochen im Jahr
         */
        loadWeeks: function() {
            showLoader();
            var objRest = new RestModelComponent({rest_url: controller.home + 'api/time/week'});
            objRest.readAll(function(objResponse, strStatus, jqXHR){
                hideLoader();
                var objData = objRest.getData();
                controller.weeks = objData.weeks;
                controller.actual_week = parseInt(objData.actual);
                console.log(objData);

                controller.weekSelection(controller.actual_week);
            });
        },

        /**
         * Select Week by Number
         * @param numWeek
         */
        weekSelection: function(numWeek) {

        }
    }
});

$(function() {
    controller.init();
});
