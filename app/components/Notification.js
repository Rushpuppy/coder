
/**
 * Notification Component
 * This Component handles Notifications
 *
 * @author: Severin Holm
 * @version: 1.0.0
 */
var NotificationComponent = function(options) {
    var $this = this;

    // Class Options
    this.options = $.extend({
    }, options);

    /**
     * Initialize WebSocket Object
     */
    this.init = function() {
        // Build Notification Container
        $('body').append($this.generateContainerHtml());
    };

    /**
     * Adding a Notification to the Notification Container
     * @param objOptions
     */
    this.addNotification = function(objOptions) {
        // Generate Default Options
        objOptions = $.extend({
            type: 'info',
            title: '',
            icon: '',
            text: '',
            delay: 5000,
            fadein_time: 1000,
            fadeout_time: 1000
        }, objOptions);

        // Set Default HTML Layout
        if(typeof(objOptions.html) == 'undefined') {
            objOptions.html = $this.generateQuickNotificationHtml(objOptions)
        }

        // Adding Quick Notification
        var strNotificationId = $this.guid();
        var strNotifiationHtml = '<span id="' + strNotificationId + '" class="hidden;">' + objOptions.html + '</span>';
        $('#notification_quick').prepend(strNotifiationHtml);

        // FadeIn and FadeOut Animation
        $('#' + strNotificationId).fadeIn(objOptions.fadein_time, function(){
            // Set TimeOut if Delay is > 0
            if(objOptions.delay > 0) {
                setTimeout(function(){
                    $('#' + strNotificationId).fadeOut(objOptions.fadeout_time, function(){
                        $('#' + strNotificationId).remove();
                    });
                }, objOptions.delay);
            }
        });
    };

    /**
     * Generates The Container HTML
     * @returns {string}
     */
    this.generateContainerHtml = function() {
        // todo: Bootstrap Responsive feature fehlt noch
        var strHtml = '<div id="notification_quick" style="position: absolute; right: 5px; top: 60px; height: auto; width: 350px;"></div>';
        return strHtml;
    };

    /**
     * Generates Quick Notification Default Layout
     * @param objOptions
     * @returns {string}
     */
    this.generateQuickNotificationHtml = function(objOptions) {
        var strHtml = '<div class="alert alert-' + objOptions.type + ' alert-dismissible" role="alert">';
        strHtml += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        strHtml += '<span style="text-transform: uppercase;">';
        strHtml += '   <span class="' + objOptions.icon + '"></span>&nbsp;<span>' + objOptions.title + '</span>';
        strHtml += '</span>';
        strHtml += '<div class="row">';
        strHtml += '   <div class="col-xs-12" style="font-size: 12px;">';
        strHtml += '      CONTENT';
        strHtml += '   </div>';
        strHtml += '</div>';
        strHtml += '</div>';
        return strHtml;
    };

    /**
     * Helper Method to create a unique Id
     * @return string
     */
    this.guid = function() {
        function s4() {return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);}
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };

    // Component Init Call
    $this.init();
};