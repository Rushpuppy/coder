
/**
 * RestModel
 * @author: Severin Holm
 * @version: 1.0.0
 */
var RestModelComponent = function(options) {
    var $this = this;

    // Model Data
    this.modelData = [];

    // Class Options
    this.options = $.extend({
        rest_url: ''
    }, options);


    this.init = function() {

    };

    /**
     * This sends a Creation POST to the Rest Interface
     * @param arrRecord
     * @param fncCallback
     */
    this.create = function(arrRecord, fncCallback) {
        // Add Record to Array
        arrRecord['id'] = 0;
        $this.modelData.push(arrRecord);
        var numArrayIndex = this.modelData.length - 1;

        // Send Record to REST API
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: $this.options.rest_url,
            dataType: "json",
            data: JSON.stringify(arrRecord),
            success: function(objResponse, strStatus, jqXHR){
                $this.modelData[numArrayIndex]['id'] = objResponse.id;
                if(typeof(fncCallback) === 'function') {
                    fncCallback(objResponse, strStatus, jqXHR);
                }
            },
            error: function(jqXHR, strStatus, errorThrown){
                // todo
            }
        });
    };

    /**
     * This reads all Json data from a Rest Interface
     * @param fncCallback
     */
    this.readAll = function(fncCallback) {
        // Send GET Request to REST API
        $.ajax({
            type: 'GET',
            url: $this.options.rest_url,
            dataType: "json",
            success: function(objResponse, strStatus, jqXHR){
                $this.modelData = objResponse;
                if(typeof(fncCallback) === 'function') {
                    fncCallback(objResponse, strStatus, jqXHR);
                }
            },
            error: function(jqXHR, strStatus, errorThrown){
                // todo
            }
        });
    };

    /**
     * This reads Json data from a Rest Interface
     * @param strField
     * @param strValue
     * @param fncCallback
     */
    this.readByFieldValue = function(strField, strValue, fncCallback) {
        // Send GET Request to REST API
        $.ajax({
            type: 'GET',
            url: $this.options.rest_url + '/' + strField + '/' + strValue,
            dataType: "json",
            success: function(objResponse, strStatus, jqXHR){
                $this.modelData = objResponse;
                if(typeof(fncCallback) === 'function') {
                    fncCallback(objResponse, strStatus, jqXHR);
                }
            },
            error: function(jqXHR, strStatus, errorThrown){
                // todo
            }
        });
    };

    /**
     * This is editing values in a Rest Interface
     * @param numId
     * @param arrRecord
     * @param fncCallback
     */
    this.update = function(numId, arrRecord, fncCallback) {
        // Update Record in Array
        $this.modelData[numId] = $.extend($this.modelData[numId], arrRecord);

        // Send PUT Request to REST API
        $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: $this.options.rest_url + '/' + numId,
            dataType: "json",
            data: JSON.stringify(arrRecord),
            success: function(objResponse, strStatus, jqXHR){
                if(typeof(fncCallback) === 'function') {
                    fncCallback(objResponse, strStatus, jqXHR);
                }
            },
            error: function(jqXHR, strStatus, errorThrown){
                // todo
            }
        });
    };

    /**
     * This is deleting values in a Rest Interface
     * @param numId
     * @param fncCallback
     */
    this.delete = function(numId, fncCallback) {
        // Delete Record in Array
        delete $this.modelData[numId];

        // Sending Delete Request to REST API
        $.ajax({
            type: 'DELETE',
            url: $this.options.rest_url + '/' + numId,
            success: function(objResponse, strStatus, jqXHR){
                if(typeof(fncCallback) === 'function') {
                    objResponse = JSON.parse(objResponse);
                    fncCallback(objResponse, strStatus, jqXHR);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                // todo
            }
        });
    };

    /**
     * Returns the Data Array
     * @returns {Array|*}
     */
    this.getData = function() {
        return $this.modelData;
    };

    // Component Init Call
    $this.init();
};