<?php
/**
 * Created by PhpStorm.
 * User: rushpuppy
 * Date: 10.05.17
 * Time: 15:23
 */

namespace app\service;


class TimeService
{
    /**
     * Gibt einen Array aller Wochen im Jahr mit den entsprechenden
     * Wochentagen und Daten zurück.
     * @param int $numYear
     * @return array
     */
    public function getWeeksOfTheYear($numYear = 0)
    {
        // Wenn kein Jahr erwünscht dann das aktuelle Jahr verwenden
        if($numYear == 0) {
            $numYear = date("Y");
        }

        $arrWeeks = [];
        $numLastWeek = $this->getLastWeekOfTheYear($numYear);
        for($numWeek = 1; $numWeek <= $numLastWeek; $numWeek++) {
            // Wochentage ermitteln
            $arrWochenTage = [];
            $strWeek = sprintf("%02d", $numWeek);
            $numFirstTag = strtotime($numYear ."W" . $strWeek);
            for($numTag = 0; $numTag < 7; $numTag++) {
                $numTagTimeStamp = $numFirstTag + ($numTag * 60 * 60 * 24);
                $arrWochenTage[$numTag] = date("Y-m-d", $numTagTimeStamp) . ' 00:00:00';
            }

            // Wochen generieren
            $arrWeeks[$numWeek] = $arrWochenTage;
        }

        return $arrWeeks;
    }

    /**
     * Alle Blocks einer Woche auslesen und zurückgeben
     * @param $numWeek
     * @return array
     */
    public function getBlocksOfWeek($numWeek)
    {
        $arrBlocks = [];
        return $arrBlocks;
    }

    /**
     * Ermittelt die letzte woche im Jahr
     * @param int $numYear
     * @return string
     */
    public function getLastWeekOfTheYear($numYear = 0)
    {
        // Wenn kein Jahr erwünscht dann das aktuelle Jahr verwenden
        if($numYear == 0) {
            $numYear = date("Y");
        }

        $objDate = new \DateTime('December 28th, ' . $numYear);
        return $objDate->format('W');
    }

    /**
     * Aktuelle Woche zurückgeben
     * @return string
     */
    public function getActualWeek() {
        $objDate = new \DateTime();
        return $objDate->format('W');
    }
}