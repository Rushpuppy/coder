<?php
/**
 * Created by PhpStorm.
 * User: rushpuppy
 * Date: 26.06.17
 * Time: 15:15
 */

namespace app\service;


class GlobalService
{
    /**
     * Save Image
     * @param $strBase64
     * @param $strFolder
     * @param $strOutputFile
     * @return string
     */
    public function convertBase64ToFile($strBase64, $strFolder, $strOutputFile)
    {
        // Album Ordner erstellen wenn dieser noch nicht existiert
        if(!file_exists($strFolder)) {
            mkdir($strFolder, 0777, true);
        }

        // Base64 File aufbereiten
        $arrBase64 = explode('base64,', $strBase64);
        $strData = base64_decode($arrBase64[1]);

        // Datei Speichern
        $strFile = $strFolder . '/' . $strOutputFile;
        $objFile = fopen( $strFile, "wb" );
        fwrite($objFile, $strData);
        fclose($objFile);
        return($strFile);
    }
}