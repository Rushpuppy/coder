<?php
/**
 * Created by PhpStorm.
 * User: rushpuppy
 * Date: 08.06.17
 * Time: 20:00
 */

namespace app\service;


class ContactService
{
    /**
     * Liefert nur die Minimal ansicht aller Kontakte
     * @param $arrContacts
     * @param $arrInfo
     * @return array
     */
    public function buildListView($arrContacts, $arrInfo)
    {
        // Order Info by ContactId
        $arrContactInfo = [];
        foreach($arrInfo as $arrRecord) {
            $numContactId = $arrRecord['resource_contact_id'];
            $strType = $arrRecord['info_type'];
            $arrContactInfo[$numContactId][$strType] = $arrRecord['info_value'];
        }

        $arrReturn = [];
        foreach($arrContacts as $arrRecord) {
            // Generate Contact
            $arrContact = [];
            $arrContact['id'] = $arrRecord['id'];

            // Build Person Data
            if($arrRecord['type'] == 'person') {
                $arrContact['title'] = $arrRecord['person_firstname'] . ' ' . $arrRecord['person_familyname'];
                $arrContact['subtitle'] = $arrRecord['person_group'];
            }

            // Build Company Data
            if($arrRecord['type'] == 'company') {
                $arrContact['title'] = $arrRecord['company_name'];
                $arrContact['subtitle'] = $arrRecord['company_branche'];
            }

            // Build Base Data
            $arrContact['address_line_1'] = $arrRecord['address_street'];
            $arrContact['address_line_2'] = $arrRecord['address_country'] . '-' . $arrRecord['address_postalcode'] . ' ' . $arrRecord['address_city'];

            // Build Contact Info
            if(isset($arrContactInfo[$arrRecord['id']]['email'])) {
                $arrContact['contact_email'] = $arrContactInfo[$arrRecord['id']]['email'];
            }
            if(isset($arrContactInfo[$arrRecord['id']]['mobile'])) {
                $arrContact['contact_phone'] = $arrContactInfo[$arrRecord['id']]['mobile'];
            } elseif(isset($arrContactInfo[$arrRecord['id']]['phone'])) {
                $arrContact['contact_phone'] = $arrContactInfo[$arrRecord['id']]['phone'];
            }

            // Build Image
            if(!empty($arrRecord['image_source'])) {
                $arrContact['image_source'] = $arrRecord['image_source'];
            }

            // Add Contact to List
            $arrReturn[] = $arrContact;
        }

        return $arrReturn;
    }

    /**
     * Liefert den Char Count in zwei Columns zurück
     * @param $arrContacts
     * @return array
     */
    public function calculateCharCounts($arrContacts)
    {
        // Initialize Alphabet Array
        $arrReturn = [];
        $arrAlphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        foreach($arrAlphabet as $numIndex => $strChar) {
            $strColumn = 'left';
            if ($numIndex & 1) {
                $strColumn = 'right';
            }
            $arrReturn[$strColumn][$strChar] = 0;
        }

        // Calculate Char Counts
        foreach($arrContacts as $arrRecord) {
            $strChar = strtoupper(substr($arrRecord['title'], 0, 1));
            if(isset($arrReturn['left'][$strChar])) {
                $arrReturn['left'][$strChar]++;
            }
            if(isset($arrReturn['right'][$strChar])) {
                $arrReturn['right'][$strChar]++;
            }
        };
        return $arrReturn;
    }

    /**
     * Setzt den Char Filter auf den Contact Array so das nur noch die
     * Kontakte angezeigt werden die dem Filter entsprechen
     * @param $arrContacts
     * @param $arrFilter
     * @return array
     */
    public function setFilter($arrContacts, $arrFilter)
    {
        $arrReturn = [];
        foreach($arrContacts as $arrContact) {
            // Apply Char Filter
            $boolAdd = true;
            if(!empty($arrFilter['char'])) {
                if(substr(strtoupper($arrContact['title']),0,1) != $arrFilter['char']) {
                    $boolAdd = false;
                }
            }

            // Add Contact to List
            if($boolAdd) {
                $arrReturn[] = $arrContact;
            }
        }
        return $arrReturn;
    }

    /**
     * Gibt die fehlenden Social Media Informationen an das GUI zurück
     * @param $arrSocialMediaData
     * @return array
     */
    public function buildSocialMediaInformations($arrSocialMediaData)
    {
        $arrResult = [];
        foreach($arrSocialMediaData as $arrRecord) {
            $arrSocialMedia = [];
            $strPlatform = $arrRecord['social_platform'];

            // FaceBook Informations
            if($strPlatform == 'facebook') {
                $arrSocialMedia['calc_url'] = 'https://facebook.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-facebook';
                $arrSocialMedia['calc_btn_icon'] = 'fa-facebook';
                $arrSocialMedia['calc_btn_text'] = 'Facebook';
            }

            // Google
            if($strPlatform == 'google') {
                $arrSocialMedia['calc_url'] = 'https://google.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-google';
                $arrSocialMedia['calc_btn_icon'] = 'fa-google-plus';
                $arrSocialMedia['calc_btn_text'] = 'Google';
            }

            // Soundcloud
            if($strPlatform == 'soundcloud') {
                $arrSocialMedia['calc_url'] = 'https://soundcloud.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-soundcloud';
                $arrSocialMedia['calc_btn_icon'] = 'fa-soundcloud';
                $arrSocialMedia['calc_btn_text'] = 'Soundcloud';
            }

            // Foursquare
            if($strPlatform == 'foursquare') {
                $arrSocialMedia['calc_url'] = 'https://foursquare.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-foursquare';
                $arrSocialMedia['calc_btn_icon'] = 'fa-foursquare';
                $arrSocialMedia['calc_btn_text'] = 'Foursquare';
            }

            // Twitter
            if($strPlatform == 'twitter') {
                $arrSocialMedia['calc_url'] = 'https://twitter.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-twitter';
                $arrSocialMedia['calc_btn_icon'] = 'fa-twitter';
                $arrSocialMedia['calc_btn_text'] = 'Twitter';
            }

            // Tumblr
            if($strPlatform == 'tumblr') {
                $arrSocialMedia['calc_url'] = 'https://tumblr.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-tumblr';
                $arrSocialMedia['calc_btn_icon'] = 'fa-tumblr';
                $arrSocialMedia['calc_btn_text'] = 'Tumblr';
            }

            // Reddit
            if($strPlatform == 'reddit') {
                $arrSocialMedia['calc_url'] = 'https://facebook.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-facebook';
                $arrSocialMedia['calc_btn_icon'] = 'fa-facebook';
                $arrSocialMedia['calc_btn_text'] = 'Facebook';
            }

            // Xing
            if($strPlatform == 'xing') {
                $arrSocialMedia['calc_url'] = 'https://xing.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'bg-green';
                $arrSocialMedia['calc_btn_icon'] = 'fa-xing';
                $arrSocialMedia['calc_btn_text'] = 'Xing';
            }

            // LinkedIn
            if($strPlatform == 'linkedin') {
                $arrSocialMedia['calc_url'] = 'https://linkedin.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-linkedin';
                $arrSocialMedia['calc_btn_icon'] = 'fa-linkedin';
                $arrSocialMedia['calc_btn_text'] = 'LinkedIn';
            }

            // Instagram
            if($strPlatform == 'instagram') {
                $arrSocialMedia['calc_url'] = 'https://instagram.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-instagram';
                $arrSocialMedia['calc_btn_icon'] = 'fa-instagram';
                $arrSocialMedia['calc_btn_text'] = 'Instagram';
            }

            // Flickr
            if($strPlatform == 'flickr') {
                $arrSocialMedia['calc_url'] = 'https://flickr.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-flickr';
                $arrSocialMedia['calc_btn_icon'] = 'fa-flickr';
                $arrSocialMedia['calc_btn_text'] = 'Flickr';
            }

            // Pinterest
            if($strPlatform == 'pinterest') {
                $arrSocialMedia['calc_url'] = 'https://pinterest.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-pinterest';
                $arrSocialMedia['calc_btn_icon'] = 'fa-pinterest';
                $arrSocialMedia['calc_btn_text'] = 'Pinterest';
            }

            // BitBucket
            if($strPlatform == 'bitbucket') {
                $arrSocialMedia['calc_url'] = 'https://bitbucket.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-bitbucket';
                $arrSocialMedia['calc_btn_icon'] = 'fa-bitbucket';
                $arrSocialMedia['calc_btn_text'] = 'BitBucket';
            }

            // GitHub
            if($strPlatform == 'github') {
                $arrSocialMedia['calc_url'] = 'https://github.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-github';
                $arrSocialMedia['calc_btn_icon'] = 'fa-github';
                $arrSocialMedia['calc_btn_text'] = 'GitHub';
            }

            // Vimeo
            if($strPlatform == 'vimeo') {
                $arrSocialMedia['calc_url'] = 'https://vimeo.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-vimeo';
                $arrSocialMedia['calc_btn_icon'] = 'fa-vimeo';
                $arrSocialMedia['calc_btn_text'] = 'Vimeo';
            }

            // Youtube
            if($strPlatform == 'youtube') {
                $arrSocialMedia['calc_url'] = 'https://youtube.com/' . $arrRecord['social_url'];
                $arrSocialMedia['calc_btn_bg'] = 'btn-pinterest';
                $arrSocialMedia['calc_btn_icon'] = 'fa-youtube';
                $arrSocialMedia['calc_btn_text'] = 'Youtube';
            }

            // Add Social Button to SocialMedia Array
            if(!empty($arrSocialMedia)) {
                $arrSocialMedia['id'] = $arrRecord['id'];
                $strHtmlBtn = '<a href="' . $arrSocialMedia['calc_url'] . '" target="_blank" class="buddy-mode-display btn btn-block btn-social ' . $arrSocialMedia['calc_btn_bg'] . '" style="margin-bottom: 10px;"><i class="fa ' . $arrSocialMedia['calc_btn_icon'] . '"></i> ' . $arrSocialMedia['calc_btn_text'] . '</a>';
                $arrSocialMedia['calc_platform'] = $strPlatform;
                $arrSocialMedia['calc_user'] = $arrRecord['social_url'];
                $arrSocialMedia['button_html'] = $strHtmlBtn;
                $arrResult[] = $arrSocialMedia;
            }
        }

        return $arrResult;
    }

    /**
     * Get All Social Platforms
     * @return array
     */
    public function getSocialMediaPlatforms()
    {
        $arrResult = [
            'facebook' => 'Facebook',
            'google' => 'Google',
            'soundcloud' => 'SoudCloud',
            'foursquare' => 'Foursquare',
            'twitter' => 'Twitter',
            'tumblr' => 'Tumblr',
            'xing' => 'Xing',
            'linkedin' => 'LinkedIn',
            'instagram' => 'Instagram',
            'flickr' => 'Flickr',
            'pinterest' => 'Pinterest',
            'bitbucket' => 'BitBucket',
            'github' => 'GitHub',
            'vimeo' => 'Vimeo',
            'youtube' => 'YouTube'
        ];
        return $arrResult;
    }

    /**
     * Get All Contact Informations
     * @return array
     */
    public function getContactInfoType()
    {
        $arrResult = [
            'mobile' => 'Mobile',
            'email' => 'Email',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'pager' => 'Pager',
            'office' => 'Office',
            'private' => 'Private',
            'other' => 'Other',
        ];
        return $arrResult;
    }
}