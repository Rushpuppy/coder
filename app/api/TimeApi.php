<?php

namespace app\api;
use app\service\TimeService;
use lib\BaseApi;

/**
 * Created by PhpStorm.
 * User: rushpuppy
 * Date: 10.05.17
 * Time: 14:45
 */
class TimeApi extends BaseApi
{

    /**
     * Default Resgister Routing
     * @param $objApp
     * @param $strRestPath
     */
    static function registerRouting($objApp, $strRestPath)
    {
        // Read
        $objApp->get($strRestPath . '/week', 'TimeApi:readWeekInformations')->add(new \lib\AuthMiddleware());

        // Create

        // Update

        // Delete
    }

    /**
     * Reading all the selected Weeks and Return to Gui
     * @return string
     */
    public function readWeekInformations()
    {
        $arrWeekInfo = [];
        $objTimeService = new TimeService();

        $arrWeekInfo['actual'] = $objTimeService->getActualWeek();
        $arrWeekInfo['weeks'] = $objTimeService->getWeeksOfTheYear();

        return $this->serialize($arrWeekInfo);
    }
}