<?php
/**
 * Created by PhpStorm.
 * User: rushpuppy
 * Date: 08.06.17
 * Time: 19:57
 */


namespace app\api;
use app\service\ContactService;
use app\service\GlobalService;
use lib\BaseApi;

class ContactApi extends BaseApi
{

    /**
     * Default Resgister Routing
     * @param $objApp
     * @param $strRestPath
     */
    static function registerRouting($objApp, $strRestPath)
    {
        // Read
        $objApp->get($strRestPath . '/filter/{filter}', 'ContactApi:readAllContact')->add(new \lib\AuthMiddleware());
        $objApp->get($strRestPath . '/id/{id}', 'ContactApi:readContactById')->add(new \lib\AuthMiddleware());
        $objApp->get($strRestPath . '/empty', 'ContactApi:readEmptyContact')->add(new \lib\AuthMiddleware());

        // Create
        $objApp->post($strRestPath . '', 'ContactApi:createContact')->add(new \lib\AuthMiddleware());

        // Update
        $objApp->put($strRestPath . '/{id}', 'ContactApi:updateContactById')->add(new \lib\AuthMiddleware());

        // Delete
        $objApp->delete($strRestPath . '/{id}', 'ContactApi:deleteContactById')->add(new \lib\AuthMiddleware());
    }

    /**
     * Reading all the Contacts from the Database and build its list view
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function readAllContact($request, $response, $args)
    {
        // Get Filter Parameter
        $strFilter = $this->getArrayValue($args, 'filter');
        $strFilter = base64_decode($strFilter, true);
        $arrFilter = json_decode($strFilter, true);

        // Build Where
        $arrWhere = [];
        $arrWhere['account_id'] = $this->session['user']['id'];
        if($arrFilter['show_company']) {
            $arrWhere['AND']['type'][] = 'company';
        }
        if($arrFilter['show_person']) {
            $arrWhere['AND']['type'][] = 'person';
        }
        $arrWhere['AND']['flag_deleted'] = 0;

        // Read Contacts From Database
        $arrContacts = $this->db->select('resource_contact', '*', $arrWhere);

        // Read Phone and Email from Contacts
        $arrContactId = [];
        foreach($arrContacts as $arrRecord) {
            $arrContactId[] = $arrRecord['id'];
        }
        unset($arrWhere['AND']['type']);
        $arrWhere['AND']['resource_contact_id'] = $arrContactId;
        $arrContactInfo = $this->db->select('resource_contact_info', '*', $arrWhere);

        // Create Listview
        $objContacts = new ContactService();
        $arrContacts = $objContacts->buildListView($arrContacts, $arrContactInfo);
        $arrCharCounts = $objContacts->calculateCharCounts($arrContacts);
        $arrContacts = $objContacts->setFilter($arrContacts, $arrFilter);

        // Build Result
        $arrResult = [];
        $arrResult['contacts'] = $arrContacts;
        $arrResult['char_counts'] = $arrCharCounts;

        return $this->serialize($arrResult);
    }

    /**
     * Reading one Contact by its ID
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function readContactById($request, $response, $args)
    {
        // Get Id Parameter
        $numId = $this->getArrayValue($args, 'id');

        // Read Contact From Database
        $arrWhere = [];
        $arrWhere['account_id'] = $this->session['user']['id'];
        $arrWhere['AND']['id'] = $numId;
        $arrWhere['AND']['flag_deleted'] = 0;
        $arrContact = $this->db->select('resource_contact', '*', $arrWhere);

        // Read Social Media From Database
        $arrWhere = [];
        $arrWhere['account_id'] = $this->session['user']['id'];
        $arrWhere['AND']['resource_contact_id'] = $numId;
        $arrWhere['AND']['flag_deleted'] = 0;
        $arrSocialMedia = $this->db->select('resource_contact_socialmedia', '*', $arrWhere);

        // Read Contact Informations From Database
        $arrWhere = [];
        $arrWhere['account_id'] = $this->session['user']['id'];
        $arrWhere['AND']['resource_contact_id'] = $numId;
        $arrWhere['AND']['flag_deleted'] = 0;
        $arrContactInfo = $this->db->select('resource_contact_info', '*', $arrWhere);

        // Convert and Build Data
        $objContacts = new ContactService();
        $arrSocialMedia = $objContacts->buildSocialMediaInformations($arrSocialMedia);
        $arrSocialPlatforms = $objContacts->getSocialMediaPlatforms();
        $arrContactTypes = $objContacts->getContactInfoType();

        // Build Return Array
        $arrReturn = $arrContact[0];
        $arrReturn['social_media'] = $arrSocialMedia;
        $arrReturn['social_media_platform'] = $arrSocialPlatforms;
        $arrReturn['contact_info'] = $arrContactInfo;
        $arrReturn['contact_info_types'] = $arrContactTypes;


        return $this->serialize($arrReturn);
    }

    /**
     * Generating empty Contact
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function readEmptyContact($request, $response, $args)
    {
        // Generate Empty Contact
        $arrReturn = [];
        $arrReturn['id'] = '';
        $arrReturn['type'] = 'person';
        $arrReturn['person_firstname'] = '';
        $arrReturn['person_familyname'] = '';
        $arrReturn['person_gender'] = 'male';
        $arrReturn['person_group'] = '';
        $arrReturn['person_dateofbirth'] = '';
        $arrReturn['person_hometown'] = '';
        $arrReturn['person_knownfrom'] = '';
        $arrReturn['company_name'] = '';
        $arrReturn['company_branche'] = '';
        $arrReturn['company_description'] = '';
        $arrReturn['address_street'] = '';
        $arrReturn['address_additional'] = '';
        $arrReturn['address_postalcode'] = '';
        $arrReturn['address_city'] = '';
        $arrReturn['address_country'] = '';
        $arrReturn['address_region'] = '';
        $arrReturn['image_source'] = 'assets/image/placeholder/no_image.png';
        $arrReturn['social_media'] = '';

        // Convert and Build Data
        $objContacts = new ContactService();
        $arrSocialPlatforms = $objContacts->getSocialMediaPlatforms();
        $arrContactTypes = $objContacts->getContactInfoType();

        $arrReturn['social_media'] = [];
        $arrReturn['social_media_platform'] = $arrSocialPlatforms;
        $arrReturn['contact_info'] = [];
        $arrReturn['contact_info_types'] = $arrContactTypes;

        return $this->serialize($arrReturn);
    }

    /**
     * REST Methode
     * Create Contact Data
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function createContact($request, $response, $args)
    {
        // Image Upload
        $strImgSrc = 'assets/image/placeholder/no_image.png';
        if(!is_null($request->getParam('image_base64'))) {
            $strBase64 = $request->getParam('image_base64');
            //$strExt = strtolower($request->getParam('image_base64'));
            $strExt = 'jpg';
            $strFolder = 'data/user_upload/' . $this->session['user']['email'];
            $strOutputFile = uniqid() . '.' . $strExt;
            $objGlobal = new GlobalService();
            $objGlobal->convertBase64ToFile($strBase64, $strFolder, $strOutputFile);
            $strImgSrc = $strFolder . '/' . $strOutputFile;
        }

        // Save Contact Record
        $arrRecord = [];
        $arrRecord['account_id'] = $this->session['user']['id'];
        $arrRecord['type'] = $request->getParam('type');
        $arrRecord['person_firstname'] = $request->getParam('person_firstname');
        $arrRecord['person_familyname'] = $request->getParam('person_familyname');
        $arrRecord['person_gender'] = $request->getParam('person_gender');
        $arrRecord['person_group'] = $request->getParam('person_group');
        $arrRecord['person_dateofbirth'] = $request->getParam('person_dateofbirth');
        $arrRecord['person_hometown'] = $request->getParam('person_hometown');
        $arrRecord['person_knownfrom'] = $request->getParam('person_knownfrom');
        $arrRecord['company_name'] = $request->getParam('company_name');
        $arrRecord['company_branche'] = $request->getParam('company_branche');
        $arrRecord['company_description'] = $request->getParam('company_description');
        $arrRecord['address_street'] = $request->getParam('address_street');
        $arrRecord['address_additional'] = $request->getParam('address_additional');
        $arrRecord['address_postalcode'] = $request->getParam('address_postalcode');
        $arrRecord['address_city'] = $request->getParam('address_city');
        $arrRecord['address_country'] = $request->getParam('address_country');
        $arrRecord['address_region'] = $request->getParam('address_region');
        $arrRecord['image_source'] = $strImgSrc;

        $arrRecord['status_created_at'] = date("Y-m-d H:i:s");
        $numId = $this->db->insert('resource_contact', $arrRecord);

        // Save Social Media Informations
        foreach($request->getParam('social_media') as $arrRecord) {
            $arrRecord['account_id'] = $this->session['user']['id'];
            $arrRecord['resource_contact_id'] = $numId;
            $arrRecord['status_created_at'] = date("Y-m-d H:i:s");
            $arrRecord['social_platform'] = $arrRecord['calc_platform'];
            $arrRecord['social_url'] = $arrRecord['calc_user'];
            unset($arrRecord['calc_platform']);
            unset($arrRecord['calc_user']);
            $this->db->insert('resource_contact_socialmedia', $arrRecord);
        }

        // Save Contact Informations
        foreach($request->getParam('contact_info') as $arrRecord) {
            $arrRecord['account_id'] = $this->session['user']['id'];
            $arrRecord['resource_contact_id'] = $numId;
            $arrRecord['status_created_at'] = date("Y-m-d H:i:s");
            $this->db->insert('resource_contact_info', $arrRecord);
        }

        $arrReturn = [];
        $arrReturn['contact_id'] = $numId;
        return $this->serialize($arrReturn);
    }

    /**
     * REST Methode
     * Saving Contact Data
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function updateContactById($request, $response, $args)
    {
        // Image Upload
        $strImgSrc = null;
        if(!is_null($request->getParam('image_base64'))) {
            $strBase64 = $request->getParam('image_base64');
            //$strExt = strtolower($request->getParam('image_base64'));
            $strExt = 'jpg';
            $strFolder = 'data/user_upload/' . $this->session['user']['email'];
            $strOutputFile = uniqid() . '.' . $strExt;
            $objGlobal = new GlobalService();
            $objGlobal->convertBase64ToFile($strBase64, $strFolder, $strOutputFile);
            $strImgSrc = $strFolder . '/' . $strOutputFile;
        }

        // Save Contact Record
        $numContactId = $request->getParam('id');
        $arrRecord = [];
        $arrRecord['type'] = $request->getParam('type');
        $arrRecord['person_firstname'] = $request->getParam('person_firstname');
        $arrRecord['person_familyname'] = $request->getParam('person_familyname');
        $arrRecord['person_gender'] = $request->getParam('person_gender');
        $arrRecord['person_group'] = $request->getParam('person_group');
        $arrRecord['person_dateofbirth'] = $request->getParam('person_dateofbirth');
        $arrRecord['person_hometown'] = $request->getParam('person_hometown');
        $arrRecord['person_knownfrom'] = $request->getParam('person_knownfrom');
        $arrRecord['company_name'] = $request->getParam('company_name');
        $arrRecord['company_branche'] = $request->getParam('company_branche');
        $arrRecord['company_description'] = $request->getParam('company_description');
        $arrRecord['address_street'] = $request->getParam('address_street');
        $arrRecord['address_additional'] = $request->getParam('address_additional');
        $arrRecord['address_postalcode'] = $request->getParam('address_postalcode');
        $arrRecord['address_city'] = $request->getParam('address_city');
        $arrRecord['address_country'] = $request->getParam('address_country');
        $arrRecord['address_region'] = $request->getParam('address_region');
        if(!is_null($strImgSrc)) {
            $arrRecord['image_source'] = $strImgSrc;
        }

        $arrRecord['status_updated_at'] = date("Y-m-d H:i:s");
        $this->db->update('resource_contact', $arrRecord, ['id' => $numContactId]);

        // Update Social Media Informations
        $arrSocialMediaId = [];
        foreach($request->getParam('social_media') as $arrRecord) {
            $arrRecord['account_id'] = $this->session['user']['id'];
            $arrRecord['resource_contact_id'] = $numContactId;
            $arrRecord['status_updated_at'] = date("Y-m-d H:i:s");
            $arrRecord['social_platform'] = $arrRecord['calc_platform'];
            $arrRecord['social_url'] = $arrRecord['calc_user'];
            unset($arrRecord['calc_platform']);
            unset($arrRecord['calc_user']);
            unset($arrRecord['calc_url']);
            unset($arrRecord['calc_btn_bg']);
            unset($arrRecord['calc_btn_icon']);
            unset($arrRecord['calc_btn_text']);
            unset($arrRecord['button_html']);

            if(isset($arrRecord['id'])) {
                $arrSocialMediaId[] = $arrRecord['id'];
                $this->db->update('resource_contact_socialmedia', $arrRecord, ['id' => $arrRecord['id']]);
            } else {
                $arrSocialMediaId[] =$this->db->insert('resource_contact_socialmedia', $arrRecord);
            }
        }

        // Delete Social Media Informations
        $this->db->delete('resource_contact_socialmedia', ['id[!]' => $arrSocialMediaId]);

        // Update Contact Informations
        $arrInfoId = [];
        foreach($request->getParam('contact_info') as $arrRecord) {
            $arrRecord['account_id'] = $this->session['user']['id'];
            $arrRecord['resource_contact_id'] = $numContactId;
            $arrRecord['status_updated_at'] = date("Y-m-d H:i:s");

            if(isset($arrRecord['id'])) {
                $arrInfoId[] = $arrRecord['id'];
                $this->db->update('resource_contact_info', $arrRecord, ['id' => $arrRecord['id']]);
            } else {
                $arrInfoId[] = $this->db->insert('resource_contact_info', $arrRecord);
            }
        }

        // Delete Contact Informations
        $this->db->delete('resource_contact_info', ['id[!]' => $arrInfoId]);

        $arrReturn = [];
        return $this->serialize($arrReturn);
    }

    /**
     * Delete Contact in Database
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function deleteContactById($request, $response, $args)
    {
        // todo:
        $arrReturn = [];
        return $this->serialize($arrReturn);
    }
}