<?php

/**
 * Controller Registration
 * Here you have to register all your Controller classes to Pimple Dependency Injection
 */
// Main Controller (Home guet or Home User)
$container['HomeController'] = function ($container) {
    if(isset($_SESSION['user'])) {
        return new \app\controller\working\TodayController($container);
    } else {
        return new \app\controller\home\HomeGuestController($container);
    }
};

// User Menu Controller (Account Settings for user)
$container['UserMenuController'] = function ($container) {
    return new \app\controller\account\UserMenuController($container);
};



// Working Today Controller
$container['TodayController'] = function ($container) {
    return new \app\controller\working\TodayController($container);
};

// Working Review Controller
$container['ReviewController'] = function ($container) {
    return new \app\controller\working\ReviewController($container);
};

// Working Summary Controller
$container['SummaryController'] = function ($container) {
    return new \app\controller\working\SummaryController($container);
};

// Working Personal Branding Controller
$container['BrandingController'] = function ($container) {
    return new \app\controller\working\BrandingController($container);
};

// Inbox Controller
$container['InboxController'] = function ($container) {
    return new \app\controller\inbox\InboxController($container);
};

// Calendar Controller
$container['CalendarController'] = function ($container) {
    return new \app\controller\calendar\CalendarController($container);
};

// Resource Contact Controller
$container['ContactController'] = function ($container) {
    return new \app\controller\resource\ContactController($container);
};

// Resource Finance Controller
$container['FinanceController'] = function ($container) {
    return new \app\controller\resource\FinanceController($container);
};

// Resource Time Controller
$container['TimeController'] = function ($container) {
    return new \app\controller\resource\TimeController($container);
};

// Resource Skill Controller
$container['SkillController'] = function ($container) {
    return new \app\controller\resource\SkillController($container);
};

// Resource Skill Controller
$container['ActionPlanController'] = function ($container) {
    return new \app\controller\resource\ActionPlanController($container);
};

// Resource Skill Controller
$container['DocumentController'] = function ($container) {
    return new \app\controller\resource\DocumentController($container);
};

// Lifespace Activity Controller
$container['ActivityController'] = function ($container) {
    return new \app\controller\lifespace\ActivityController($container);
};

// Lifespace Delegation Controller
$container['DelegationController'] = function ($container) {
    return new \app\controller\lifespace\DelegationController($container);
};

// Lifespace Incubator Controller
$container['IncubatorController'] = function ($container) {
    return new \app\controller\lifespace\IncubatorController($container);
};

// Lifespace Meeting Controller
$container['MeetingController'] = function ($container) {
    return new \app\controller\lifespace\MeetingController($container);
};

// Lifespace Project Controller
$container['ProjectController'] = function ($container) {
    return new \app\controller\lifespace\ProjectController($container);
};

// Lifespace Task Controller
$container['TaskController'] = function ($container) {
    return new \app\controller\lifespace\TaskController($container);
};

// Lifespace Tracker Controller
$container['TrackerController'] = function ($container) {
    return new \app\controller\lifespace\TrackerController($container);
};

/**
 * Service Registration
 * Here you have to register all your Service classes to Pimple Dependency Injection
 */
// Account Service
$container['AccountService'] = function ($container) {
    return new \app\service\AccountService($container);
};


/**
 * REST API Registration
 */
// Time REST API
$container['TimeApi'] = function ($container) {
    return new \app\api\TimeApi($container);
};

// Contact REST API
$container['ContactApi'] = function ($container) {
    return new \app\api\ContactApi($container);
};